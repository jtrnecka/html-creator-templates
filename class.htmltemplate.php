<?php
require './class.htmlentity.php';
/**
 * Abstract class with final functions which allows advanced html operations.
 *
 * @author Jiri Trnecka
 */
abstract class HTMLTemplate {
    
    /**
     * Abstract function for generate html of the string.
     */
    abstract function getHTML();
    
    /**
     * Saving the html of the string into the file.
     * 
     * @param String $filename
     * @throws Exception
     */
    public function saveHtmlToFile($filename) {
        if (file_exists($filename)) {
            throw new Exception("$filename can not create. The file is exists.");
        } else {
            $file = fopen($filename, "w");
            fwrite($file, $this->getHTML());
            fclose($file);
        }
    }
}
