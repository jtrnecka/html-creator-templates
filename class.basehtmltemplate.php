<?php
require './class.htmltemplate.php';

/**
 * The generating basic html template
 *
 * @author Jiri Trnecka
 */
class BaseHTMLTemplate extends HTMLTemplate {
    
    /**
     * The title of the page
     * @var String 
     */
    public $title = '';
    
    /**
     * The content of the page 
     * @var String 
     */
    public $content = '';
    
    /**
     * Meta tags 
     * @var array 
     */
    public $meta = array();
    
    /**
     * Charset of the page (default is utf8)
     * @var String
     */
    public $charset = "utf8";
    
    /**
     * Insert script into the header
     * @var array
     */
    public $script_head = array();
    
    /**
     * Insert script into the body
     * @var array
     */
    public $script_body = array();
    
    /**
     * Insert link into the header
     * @var array 
     */
    public $link_file = array();
    
    const TYPE_TEXT_JAVASCRIPT = "text/javascript";
    
    const TYPE_TEXT_ECMASCRIPT = "text/ecmascript";
    
    const TYPE_APPLICATION_JAVASCRIPT = "application/javascript";
    
    const TYPE_APPLICATION_ECMASCRIPT = "application/ecmascript";
    
    
    /**
     * Setting of the title 
     * @param type $title
     * @return \BaseHTMLTemplate
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    
    /**
     * Gets the title of the page
     * @return String
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * Gets the content of the page
     * @return String
     */
    public function getContent() {
        return $this->content;
    }
    
    /**
     * Add content generate using function getHTML()
     * @param String $content
     */
    public function addContent($content) {
        $this->content .= $content;
    }
    
    /**
     * Add meta tag
     * @param String $name
     * @param String $content
     */
    public function addMeta($name, $content) {
        $this->meta[$name] = $content;
    }
    
    /**
     * Sets the charset of the page (default is utf8)
     * @param String $charset
     */
    public function setCharset($charset = "utf8") {
        $this->charset = $charset;
    }
    
    /**
     * Add script file in the head of the top page or bottom page
     * 
     * Varible $type has constants: 
     *      TYPE_TEXT_JAVASCRIPT = "text/javascript"
     *      TYPE_TEXT_ECMASCRIPT = "text/ecmascript"
     *      TYPE_APPLICATION_JAVASCRIPT = "application/javascript"
     *      TYPE_APPLICATION_ECMASCRIPT = "application/ecmascript"
     * 
     * @param String $src
     * @param String $place default is "top"
     * @param String $type default is "text/javascript"
     */
    public function addScriptFile($src, $place = "top", $type = 'text/javascript') {
        $script = new HTMLEntity("script", true);
        $script->setAttributes(array("src"=>$src, "type"=>$type));
        if ($place == "top") {
            $this->script_head[] = $script->getHTML();
        } else {
            $this->script_body[] = $script->getHTML();
        }
    }
    
    public function addLinkFile($href, $rel = "stylesheet", $type = "text/css") {
        $link = new HTMLEntity("link", false);
        $link->setAttributes(array(
            "rel"=>$rel,
            "type"=>$type,
            "href"=>$href
        ));
        $this->link_file[] = $link->getHTML();
    }
    
    /**
     * Get basic html of the page as String type
     * @return String
     */
    public function getHTML() {
        $title = new HTMLEntity("title");
        $title->addContent($this->title);
        
        // generate meta tags
        $meta = new HTMLEntity("meta", false);
        $meta->setAttributes($this->meta);
        
        // charset
        $charset = new HTMLEntity("meta", false);
        $charset->setAttributes(array("charset"=>$this->charset));
        
        $head = new HTMLEntity("head");
        $head->addContent($charset->getHTML());
        $head->addContent($meta->getHTML());
        $head->addContent($title->getHTML());
        
        // add link file to the head
        foreach ($this->link_file as $link) {
            $head->addContent($link);
        }
        
        // add javascript to the head
        foreach( $this->script_head as $script) {
            $head->addContent($script);
        }
        
        $body = new HTMLEntity("body");
        $body->addContent($this->content);
        
        // add javascript to the body
        foreach( $this->script_body as $script) {
            $body->addContent($script);
        }        
        
        $html = new HTMLEntity("html");
        $html->addContent($head->getHTML());
        $html->addContent($body->getHTML());
        return "<!DOCTYPE html>" . $html->getHTML();
    }

}
