<?php
// basic example page


require './class.basehtmltemplate.php';
$html = new BaseHTMLTemplate();
$html->title = "Generovaná stránka pomocí PHP";
$html->content = "<b>Obsah stránky který nemusí být pouze text.</b>";
$html->addContent("<p>Dále lze přidávat text i tagy pomocí addContent().</p>");
$html->addMeta("author", "Jiří Trnečka");
$html->addMeta("description", "Popis stránky");
echo $html->getHTML();
