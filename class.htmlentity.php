<?php

class HTMLEntity {

    private $tagName = '';
    private $attributes = array();
    private $content = '';
    private $isPair;
    private $htmlString;

    /**
     * Create HTML entity.
     *
     * @param String $tagName Name of tag
     * @param Boolean $isPair If the tag is pair, return true.
     * @author Jiri Trnecka
     */
    public function __construct($tagName, $isPair = true) {
        $this->tagName = $tagName;
        $this->isPair = $isPair;
    }

    /**
     * Set the attributes of the tag
     * @param array $attributes
     */
    public function setAttributes(array $attributes) {
        $this->attributes = $attributes;
    }

    /**
     * Get the attributes in the string format
     * @return array
     */
    public function getAttributes() {
        return $this->attributes;
    }

    /**
     * Get the content in the string format
     * @return String
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Add content in the string format. It depends on the order of.
     * @param String $content
     */
    public function addContent($content) {
        $this->content .= $content;
    }

    /**
     * Return HTML for tag in the string format.
     * @return String
     */
    public function getHTML() {
        $attr = '';
        foreach ($this->attributes as $attribute => $value) {
            if (is_numeric($value)) {
                $attr = $attr . ' ' . $attribute . '=' . $value;
            } else {
                $attr = $attr . ' ' . $attribute . '="' . $value . '"';
            }
        }

        if ($this->isPair) {
            $this->htmlString = '<' . $this->tagName . ' ' . $attr . '>' . $this->content . '</' . $this->tagName . '>';
        } else {
            $this->htmlString = '<' . $this->tagName . ' ' . $attr . ' >';
        }
        return $this->htmlString;
    }

}
